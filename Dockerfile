FROM [% DOCKER_IMAGE %]

COPY [ "[% PWD %]", "/web" ]
COPY [ "[% SHARED_CONFIG_DIR %]", "/etc/shared/config" ]

VOLUME /root/.cpanm
VOLUME /web/.cpanm
VOLUME /var/log/[% PROJECT_NAME %]
VOLUME /var/run

WORKDIR /web

ENV TERM   xterm
ENV USER   [% USER %]
ENV EDITOR [% TEXT_EDITOR %]

[% IF LD_LIBRARY_PATH %]
ENV LD_LIBRARY_PATH    [% LD_LIBRARY_PATH %],
[% END %]

[% IF CARTON_MIRROR_URL %]
ENV PERL_CARTON_MIRROR [% CARTON_MIRROR_URL %]
ENV PERL_CPANM_OPT --mirror [% CARTON_MIRROR_URL %] --mirror http://cpan.metacpan.org --mirror -v -L local
[% END %]

[% IF EXPOSE_PORTS_WITH_COMMA_DELIMITED %]
    [% FOREACH pair_of_ports IN EXPOSE_PORTS_WITH_COMMA_DELIMITED.split(",") %]
        [% FOREACH pair_of_port IN pair_of_ports.split(":") %]
EXPOSE [% pair_of_port.1 %]
        [% END %]
    [% END %]
[% END %]

RUN /web/tools/init-docker-container [% USER %] [% UID %]
RUN carton install; carton update

RUN ln -s /web /[% PROJECT_NAME %]
RUN cp /web/config/munner/config/production.yml /web/munner.yml

RUN chown -R [% USER %] /var/log/[% PROJECT_NAME %]
RUN chown -R [% USER %] /var/run/[% PROJECT_NAME %]
RUN chown -R [% USER %] /web

CMD [ "/web/tools/my_init", "--", "bash", "-c", "munner duck --group bootstrap; perl -e sleep" ]
