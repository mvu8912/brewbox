# Template for Perl Project Development #

Start from something better than nothing.
This template will guide you through from zero to hero.

You don't need to install any special system packages and the version of perl where your project would be run on.

All you need to do is run "tools/container" command, which will ask you a bundle of questions that are related to the new project.

At the end the template will automaticlly turns into a workable project folder.

# What problems it try to solve #

Every developer has they own way to develop project and most of us are doing differently or do it wrong.

For example:

1. How do we manage different perl version? system perl, Perlbrew, plenv?? thing is they needs time to get installed and maintain.

2. How do you keep your perl version up to dated without install a new perl across all the environment (100xdev, 2xstage, 2xprod)? This is pain. very pain and scary and risky. Right?

3. How do we manage dependancy? use cpanm or cpan or carton? install to the system? install to the perlbrew/plenv perl?

4. How to pack the package and release to cpan or pinto for sharing your idea? Makefile.PL, or Dist::Zilla?

# AIM TO #

This template / framework will level our way to do thing at the same way.

If we want to install new module, added it to `cpanfile` and run `carton install`. **AS USUAL**

If we want to release to CPAN. run `carton exec dzil release`. **AS USUAL**

If we want to send to pinto. run `carton exec dzil build` and `carton exec pinto add <file>.tar.gz`. **AS USUAL**

If we want to run prove. call `carton exec prove -lr t`. **AS USUAL**

If we want to satisfy our curiosity what is running inside the container? call `container inside`. **YOU ARE INSIDE Millennium Falcon**

Not enough, you want more power inside the container? call `container inside ~~root`. **TRY ANY THING THERE. EVEN HAMFUL `rm -fr /`**

If the container is broken. restart it `container restart`. **BACK TO NORMAL IN NO TIME**

If you run things in Jenkins, but having issue with tty. easy, call `carton exec ~~no-tty perl -I lib bin/something.pl` **AS USUAL**

If you have a shared environment with all the containers in the same box? for example: 2 jenkins jobs, one for dev and one for stage release. if dev start run something in container `jenkins-foobar-container` and stage run something too, but hangon. is that the going to use the same container name. Like 2 cars crash together in the highway. OMG!

No worry! you can specify the container prefix to avoid that conflict.

for dev: You run `carton exec ~~container-prefix=dev perl something.pl`

for stage: You run `carton exec ~~container-prefix=stage perl something.pl`

Problem solved!

Anymore question, please email me.

If you want to contribute, please fork this repo. Thanks

# ROAD MAP #

Currently, this template system will only support new project start from scratch.

If you have any existing project, you will need to do some work. Or email me for consultation.

More consultation, I will know how is the best way to add feature to the system to adopt exist project automatically.

For deploy or release, could be complicated. But as we start using this template. We could also level everybody using the same way to deply in the future.
 
## All you need in your machine are ##

1. Git - (type `git` in the terminal to check if it is installed)
2. System Perl - ( type `perl -v` in the terminal to check if it is installed )

## Use these steps as your project starting point ##

1. mkdir ~/web
2. cd ~/web
2. git clone https://mvu8912@bitbucket.org/mvu8912/brewbox.git <project-folder>
3. cd <project-folder>
4. export PATH=tools:bin:$PATH
4. container start

### ps1. when you run container command. your current directory location must be inside the project folder. ###

### ps2. when you run container command. do not change current directory location into tools folder. Otherwise, it won't work. ###

# How it works #

The whole idea is using docker to do all the heavy lifting (system packages, perl version and running environment)

The template system will just take you to the right starting point seamlessly.

You won't notice you are using docker or vagrant at all.

The user will run carton and cpanm command like usual.

When you call `carton exec perl bin/yoursecript.pl`

the `carton` script is actually a wrapper of `container exec carton exec perl bin/yourscript.pl`

and `container` script is a wrapper of the actual `docker exec -it <container-name> carton exec perl bin/yourscript.pl`

The good thing about these wrapper is that it takes care all the smart logic and admin tasks for you.

So we can focus on our work effectively.
