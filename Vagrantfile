## vim: syntax=ruby

dns = ENV["DNS"] || "__DNS_SERVER__"

if dns
    dns_args = ["--dns", dns]
else
    dns_args = []
end

ports = "__EXPOSE_PORTS_WITH_COMMA_DELIMITED__".split(",")

if !ENV["CONTAINER_PREFIX"]
    ENV["CONTAINER_PREFIX"] = "beta"
end

Vagrant.configure("2") do |config| 
    config.vm.define ENV["USER"] + "-" + ENV["CONTAINER_PREFIX"] + "-__PROJECT_NAME__" do |v| 
        v.vm.provider "docker" do |d| 
            d.name    = ENV["USER"] + "-" + ENV["CONTAINER_PREFIX"] + "-__PROJECT_NAME__"
            d.image   = "__DOCKER_IMAGE__"
            d.volumes = [ 
                ENV["PWD"] + ":/web:rw",
                "/root/.cpanm",
                "/web/.cpanm",
            ]

            ## carton mirror username: __CARTON_MIRROR_USERNAME__
            ## carton mirror password: __CARTON_MIRROR_PASSWORD__
            ## carton mirror hostname: __CARTON_MIRROR_HOSTNAME__

            d.env = { 
                USER:               ENV["USER"],
                TERM:               "xterm",
                EDITOR:             "__TEXT_EDITOR__",
                LD_LIBRARY_PATH:    "__LD_LIBRARY_PATH__",
                PINTO_USERNAME:       "__PINTO_USERNAME__",
                PINTO_REPOSITORY_ROOT: "__CARTON_MIRROR_URL__",
                PERL_CARTON_MIRROR: "__CARTON_MIRROR_URL__",
                PERL_CPANM_OPT:     "--mirror __CARTON_MIRROR_URL__ --mirror http://cpan.metacpan.org --mirror-only -v -L local",
            }

            if ports
                d.ports = ports
            end
            
            d.create_args = [ "--hostname", d.name, "--privileged" ] | dns_args
            
            d.cmd = [ "/web/tools/my_init", "--", "bash", "-c",
                [
                    [ "/web/tools/init-docker-container", ENV["USER"], Process.uid.to_s, " 2>&1 1>/web/tools/init.log" ].join(" "),
                    [ "exec", "perl", "-e", "sleep; ## docker container placeholder" ].join(" "),
                ].join(";")
            ]
        end 
    end 
end
